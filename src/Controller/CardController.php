<?php

namespace App\Controller;

use App\Entity\Card;
use App\Service\CardService;
use App\Service\ListService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Serializer\SerializerInterface;

#[Route('/card')]
class CardController extends AbstractController
{
    public function __construct(
        private CardService $cardService,
        private EntityManagerInterface $entityManager,
        private ListService $listService,
        private SerializerInterface $serializer
    ) {
    }

    #[Route('/updateCardContent', name: 'card_update_content')]
    public function updateCardContent(Request $request): JsonResponse
    {
        $data = json_decode($request->getContent());

        $cardId = $data->cardId;
        $newContent = $data->newContent;

        $card = $this->cardService->getCard($cardId);

        $card->setContent($newContent);
        $this->entityManager->persist($card);
        $this->entityManager->flush();

        return new JsonResponse(['success' => true, 'card' => $card]);
    }

    #[Route('/updateCardList', name: 'card_update_list')]
    public function updateCardList(Request $request): JsonResponse
    {
        $data = json_decode($request->getContent());

        $currentCardId = $data->currentCard;
        $targetListId = $data->targetList;

        $currentCard = $this->cardService->getCard($currentCardId);
        $targetList = $this->listService->getList($targetListId);

        $currentCard->setListInBoard($targetList);
        $this->entityManager->persist($currentCard);
        $this->entityManager->flush();

        return new JsonResponse(['success' => true, 'currentCard' => $currentCard]);
    }

    #[Route('/addCard', name: 'card_add')]
    public function addCard(Request $request): JsonResponse
    {
        $data = json_decode($request->getContent());

        $cardTitle = $data->cardTitle;
        $targetListId = $data->targetList;

        $targetList = $this->listService->getList($targetListId);
        $card = new Card();

        $card->setTitle($cardTitle);
        $card->setListInBoard($targetList);
        $this->entityManager->persist($card);
        $this->entityManager->flush();

        $cardData = [
            'id' => $card->getId(),
            'title' => $card->getTitle()
        ];

        return new JsonResponse(['success' => true, 'card' => $cardData]);
    }
}
