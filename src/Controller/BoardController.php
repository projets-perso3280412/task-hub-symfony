<?php

namespace App\Controller;;

use App\Entity\Board;
use App\Form\BoardType;
use App\Service\BoardService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

#[Route('/board')]
class BoardController extends AbstractController
{
    public function __construct(private BoardService $boardService, private EntityManagerInterface $entityManagerInterface)
    {
    }

    #[Route('/add', name: 'board_add')]
    public function add(Request $request): Response
    {
        $board = new Board();
        return $this->boardHandler($board, $request);
    }

    #[Route('/update/{id}', name: 'board_update')]
    public function update(Board $board, Request $request): Response
    {
        return $this->boardHandler($board, $request);
    }

    #[Route('/delete/{id}', name: 'board_delete')]
    public function delete(Board $board): RedirectResponse
    {
        $this->entityManagerInterface->remove($board);
        $this->entityManagerInterface->flush();

        return $this->redirectToRoute('board_get_all');
    }

    private function boardHandler(Board $board, Request $request): Response|RedirectResponse
    {
        $form = $this->createForm(BoardType::class, $board);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $board->setUser($this->getUser());
            $this->entityManagerInterface->persist($board);
            $this->entityManagerInterface->flush();

            return $this->redirectToRoute('board_get_all');
        }

        return $this->render('form/formBoard.html.twig', [
            'form' => $form
        ]);
    }
}
