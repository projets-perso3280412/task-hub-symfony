<?php

namespace App\Controller;

use App\Entity\Board;
use App\Service\BoardService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

#[Route('/kanban')]
class KanbanController extends AbstractController
{
    public function __construct(private BoardService $boardService, private EntityManagerInterface $entityManager)
    {
    }

    #[Route('/', name: 'board_get_all')]
    public function getAll(): Response
    {
        $boards = $this->boardService->getBoards();

        return $this->render('kanban/index.html.twig', [
            'boards' => $boards
        ]);
    }

    #[Route('/board/{id}', name: 'board_get_one')]
    public function getOne(Board $board): Response
    {
        $board = $this->boardService->getBoard($board);

        return $this->render('kanban/kanbanDetail.html.twig', [
            'board' => $board
        ]);
    }
}
