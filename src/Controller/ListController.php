<?php

namespace App\Controller;

use App\Entity\ListInBoard;
use App\Service\BoardService;
use App\Service\ListService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Attribute\Route;

#[Route('/list')]
class ListController extends AbstractController
{
    public function __construct(
        private ListService $listService,
        private EntityManagerInterface $entityManager,
        private BoardService $boardService
    ) {
    }

    #[Route('/addList', name: 'list_add')]
    public function addList(Request $request): JsonResponse
    {
        $data = json_decode($request->getContent());

        $listTitle = $data->listTitle;
        $boardId = $data->boardId;

        $board = $this->boardService->getBoard($boardId);
        $list = new ListInBoard();

        $list->setTitle($listTitle);
        $list->setBoard($board);
        $this->entityManager->persist($list);
        $this->entityManager->flush();

        $listData = [
            'id' => $list->getId(),
            'title' => $list->getTitle()
        ];

        return new JsonResponse(['success' => true, 'list' => $listData]);
    }

    #[Route('/updateListTitle', name: 'list_update_title')]
    public function updateListTitle(Request $request): JsonResponse
    {
        $data = json_decode($request->getContent());

        $listTitle = $data->listTitle;
        $listId = $data->listId;

        $list = $this->listService->getList($listId);

        $list->setTitle($listTitle);
        $this->entityManager->persist($list);
        $this->entityManager->flush();

        $listData = [
            'id' => $list->getId(),
            'title' => $list->getTitle()
        ];

        return new JsonResponse(['success' => true, 'list' => $listData]);
    }
}
