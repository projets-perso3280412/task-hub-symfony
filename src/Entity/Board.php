<?php

namespace App\Entity;

use App\Repository\BoardRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: BoardRepository::class)]
class Board
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $title = null;

    #[ORM\ManyToOne(inversedBy: 'boards')]
    #[ORM\JoinColumn(nullable: false)]
    private ?User $user = null;

    #[ORM\OneToMany(targetEntity: ListInBoard::class, mappedBy: 'board', orphanRemoval: true)]
    private Collection $listsInBoard;

    public function __construct()
    {
        $this->listsInBoard = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): static
    {
        $this->title = $title;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): static
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Collection<int, ListInBoard>
     */
    public function getListsInBoard(): Collection
    {
        return $this->listsInBoard;
    }

    public function addListInBoard(ListInBoard $listInBoard): static
    {
        if (!$this->listsInBoard->contains($listInBoard)) {
            $this->listsInBoard->add($listInBoard);
            $listInBoard->setBoard($this);
        }

        return $this;
    }

    public function removeListInBoard(ListInBoard $listInBoard): static
    {
        if ($this->listsInBoard->removeElement($listInBoard)) {
            // set the owning side to null (unless already changed)
            if ($listInBoard->getBoard() === $this) {
                $listInBoard->setBoard(null);
            }
        }

        return $this;
    }
}
