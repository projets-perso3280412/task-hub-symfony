<?php

namespace App\Repository;

use App\Entity\ListInBoard;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<ListInBoard>
 *
 * @method ListInBoard|null find($id, $lockMode = null, $lockVersion = null)
 * @method ListInBoard|null findOneBy(array $criteria, array $orderBy = null)
 * @method ListInBoard[]    findAll()
 * @method ListInBoard[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ListInBoardRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ListInBoard::class);
    }

//    /**
//     * @return ListInBoard[] Returns an array of ListInBoard objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('l')
//            ->andWhere('l.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('l.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?ListInBoard
//    {
//        return $this->createQueryBuilder('l')
//            ->andWhere('l.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
