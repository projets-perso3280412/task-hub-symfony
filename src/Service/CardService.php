<?php

namespace App\Service;

use App\Entity\Card;
use Doctrine\ORM\EntityManagerInterface;

class CardService
{
    public function __construct(private readonly EntityManagerInterface $entityManager)
    {
    }

    public function getCards()
    {
        return $this->entityManager->getRepository(Card::class)->findAll();
    }

    public function getCard(int|Card $card)
    {
        return $this->entityManager->getRepository(Card::class)->find($card);
    }
}
