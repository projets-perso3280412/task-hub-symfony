<?php

namespace App\Service;

use App\Entity\ListInBoard;
use Doctrine\ORM\EntityManagerInterface;

class ListService
{
    public function __construct(private readonly EntityManagerInterface $entityManager)
    {
    }

    public function getLists()
    {
        return $this->entityManager->getRepository(ListInBoard::class)->findAll();
    }

    public function getList(int|ListInBoard $listInBoard)
    {
        return $this->entityManager->getRepository(ListInBoard::class)->find($listInBoard);
    }
}
