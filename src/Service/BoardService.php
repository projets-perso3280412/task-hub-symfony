<?php

namespace App\Service;

use App\Entity\Board;
use Doctrine\ORM\EntityManagerInterface;

class BoardService
{
    public function __construct(private readonly EntityManagerInterface $entityManager)
    {
    }

    public function getBoards()
    {
        return $this->entityManager->getRepository(Board::class)->findAll();
    }

    public function getBoard(int|Board $board)
    {
        return $this->entityManager->getRepository(Board::class)->find($board);
    }
}
