import { Controller } from "@hotwired/stimulus";

export default class extends Controller {
  static targets = ["idToDelete", "deleteConfirmButton", "title"];

  confirmDelete(event) {
    this.idToDeleteTarget.innerHTML = event.params.id;
    this.titleTarget.innerHTML = "<b>Title: </b>" + event.params.title;
  }

  fillHref() {
    const divContent = this.idToDeleteTarget.innerHTML;
    const link = this.deleteConfirmButtonTarget;
    link.href = "delete/" + divContent;
  }
}
