import { Controller } from "@hotwired/stimulus";

export default class extends Controller {
  connect() {}

  updateCardContent(event) {
    event.preventDefault();

    const textArea = document.getElementById(
      "cardContent-" + event.params.cardId
    );

    let newContent = textArea.value;
    console.log(newContent);
    const path = $("#pathModifyContent").attr("data-path");

    $.ajax({
      url: path,
      method: "POST",
      processData: false,
      contentType: "application/json; charset=utf-8",
      data: JSON.stringify({
        newContent: newContent,
        cardId: event.params.cardId,
      }),
      dataType: "json",
      success: function (data) {
        console.log(data);
      },
    });
  }

  addCard(event) {
    event.preventDefault();
    const targetList = $("#selectedList").val();
    const cardTitle = $("#card-input").val();

    if (cardTitle) {
      const path = $("#add-card").attr("data-path");

      $.ajax({
        url: path,
        method: "POST",
        processData: false,
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify({
          targetList: targetList,
          cardTitle: cardTitle,
        }),
        dataType: "json",
        success: function (data) {
          console.log(data);
          window.location.reload();
        },
      });
    }
  }

  addList(event) {
    event.preventDefault();
    const listTitle = $("#list-input").val();
    const boardId = event.params.boardId;

    if (listTitle) {
      const path = $("#add-list").attr("data-path");

      $.ajax({
        url: path,
        method: "POST",
        processData: false,
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify({
          listTitle: listTitle,
          boardId: boardId,
        }),
        dataType: "json",
        success: function (data) {
          console.log(data);
          window.location.reload();
        },
      });
    }
  }

  updateListTitle(event) {
    event.preventDefault();
    const editableTitle = document.getElementById(
      "listTitle-" + event.params.listId
    );
    const inputField = document.createElement("input");
    const currentTitle = event.params.listTitle;
    inputField.type = "text";
    inputField.value = currentTitle;

    editableTitle.replaceWith(inputField);
    inputField.focus();

    inputField.addEventListener("blur", function () {
      const newText = this.value.trim();
      const newElement = document.createElement("h5");

      newElement.classList.add("card-title", "list-title");
      newElement.setAttribute("id", "listTitle-" + event.params.listId);
      newElement.setAttribute(
        "data-action",
        "click->kanban--kanban-update#updateListTitle"
      );
      newElement.setAttribute(
        "data-kanban--kanban-update-list-id-param",
        event.params.listId
      );
      newElement.setAttribute(
        "data-kanban--kanban-update-list-title-param",
        newText
      );
      newElement.textContent = newText;

      if (newText) {
        this.replaceWith(newElement);
        const path = $("#pathUpdateListTitle").attr("data-path");
        console.log(path);

        $.ajax({
          url: path,
          method: "POST",
          processData: false,
          contentType: "application/json; charset=utf-8",
          data: JSON.stringify({
            listTitle: newText,
            listId: event.params.listId,
          }),
          dataType: "json",
          success: function (data) {
            console.log(data);
            window.location.reload();
          },
        });
      }
    });

    inputField.addEventListener("keypress", function (event) {
      if (event.key === "Enter") {
        this.blur();
      }
    });
  }
}
