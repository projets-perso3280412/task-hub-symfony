import { Controller } from "@hotwired/stimulus";

export default class extends Controller {
  connect() {
    const draggables = document.querySelectorAll(".kanban-card");
    const droppables = document.querySelectorAll(".kanban-list");
    let lastCard = null;

    draggables.forEach((card) => {
      card.addEventListener("dragstart", () => {
        card.classList.add("is-dragging");
      });
      card.addEventListener("dragend", () => {
        card.classList.remove("is-dragging");
        lastCard = card.id;
      });
    });

    droppables.forEach((list) => {
      list.addEventListener("dragover", (e) => {
        e.preventDefault();

        const bottomCard = this.insertAboveCard(list, e.clientY);
        const currentCard = document.querySelector(".is-dragging");

        if (!bottomCard) {
          list.appendChild(currentCard);
        } else {
          list.insertBefore(currentCard, bottomCard);
        }
      });

      list.addEventListener("dragend", (e) => {
        e.preventDefault();

        const targetList = list.id;
        console.log("list: " + targetList + " and card: " + lastCard);
        const path = $("#pathUpdateListOfCard").attr("data-path");

        $.ajax({
          url: path,
          method: "POST",
          processData: false,
          contentType: "application/json; charset=utf-8",
          data: JSON.stringify({
            currentCard: lastCard,
            targetList: targetList,
          }),
          dataType: "json",
          success: function (data) {
            console.log(data);
          },
        });
      });
    });
  }

  insertAboveCard(list, mouseY) {
    const cards = list.querySelectorAll(".kanban-card:not(.is-dragging)");

    let closestCard = null;
    let closestOffset = Number.NEGATIVE_INFINITY;

    cards.forEach((card) => {
      const { top } = card.getBoundingClientRect();

      const offset = mouseY - top;

      if (offset < 0 && offset > closestOffset) {
        closestOffset = offset;
        closestCard = card;
      }
    });

    return closestCard;
  }
}
